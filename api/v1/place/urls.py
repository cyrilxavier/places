from django.conf.urls import url
from place import views


app_name = ["place"]

urlpatterns = [
	url(r'^place-list/',views.place_list, name="place_list"),
]