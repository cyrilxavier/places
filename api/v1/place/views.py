from place.models import Place
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.response import Response
from main.functions import get_auto_id
from rest_framework.permissions import AllowAny, IsAuthenticated
from api.v1.place.serializers import PlaceSerializer
from api.v1.general.functions import generate_serializer_errors
from rest_framework import status


@api_view(['GET'])
@permission_classes((AllowAny,))
@renderer_classes((JSONRenderer,))
def place_list(request):
   
    instances = Place.objects.filter(is_deleted=False)

    query = request.GET.get('q')
    if query:
        instances = instances.filter(name__istartswith=query)
        
    serialized = PlaceSerializer(instances,many=True,context={"request":request})

    response_data = {
        "StatusCode" : 6000,
        'data' : serialized.data
    }

    return Response(response_data, status=status.HTTP_200_OK)




