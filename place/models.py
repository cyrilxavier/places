from django.db import models
from django.utils.translation import ugettext_lazy as _
import uuid


class Place(models.Model):
    name = models.CharField(max_length=128,blank=True,null=True)
    image = models.ImageField(upload_to="web/")
    is_featured = models.BooleanField(default=False)

    class Meta:
        db_table = 'places_place'
        verbose_name = _('place')
        verbose_name_plural = _('places')

    def __str__(self):
        return self.name