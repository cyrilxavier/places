from place import views
from django.conf.urls import url
from django.urls import path


app_name=["place"]

urlpatterns = [

    path(r'^place/$', views.place_list, name='place_list'),
]