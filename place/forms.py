from django import forms
from django.forms.widgets import TextInput, Select, Textarea, HiddenInput
from places.models import Place
from django.utils.translation import ugettext_lazy as _


class PlaceForm(forms.ModelForm):
    
    class Meta:
        model = Place
        fields = ["name","is_featured"]
        widgets = {
            "name": TextInput(
                attrs = {
                    "class": "required form-control extra",
                    "placeholder" : "Name",
                }
            )              
        }
        error_messages = {
            "name" : {
                "required" : _("name field is required.")
            }
        }
        help_texts = {
        }
        
    def clean(self):
        cleaned_data = super(PlaceForm, self).clean()
        return cleaned_data