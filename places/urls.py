from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path


urlpatterns = [
    path('admin/', admin.site.urls),
    path('places/', include('place.urls', namespace='places')),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^api/v1/place/', include('api.v1.place.urls', namespace='api_v1_place')),
]
